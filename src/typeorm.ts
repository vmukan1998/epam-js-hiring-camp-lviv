import "reflect-metadata";
import { createConnection } from "typeorm";

import User from "./entities/User";

console.log(__dirname + "\\entities");

createConnection({
  type: "mysql",
  host: process.env.MYSQL_HOST,
  port: parseInt(process.env.MYSQL_PORT),
  username: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  database: process.env.MYSQL_DB,
  entities: [
    __dirname + "\\entities"
  ],
  logging: true,
  synchronize: true,
})
  .then(async connection => { 
    const repo = connection.getRepository(User); 
    const users = await repo.find();

    console.log('users', );
  })
   
  .catch(err => console.log(`TypeORM connection error: ${err}`));


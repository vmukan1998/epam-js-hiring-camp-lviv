import { Entity, PrimaryColumn, Column } from "typeorm";

@Entity()
export default class User {
  @PrimaryColumn()
  public id: number;

  @Column()
  public name: string;

  @Column()
  public status: string;
}

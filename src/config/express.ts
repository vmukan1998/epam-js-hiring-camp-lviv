import { Request, Response } from "express";
import * as express from "express";
import * as bodyParser from "body-parser";
import * as logger from "morgan";

import router from "../routes";

const app = express();

app.use(bodyParser.json());
app.use(logger("dev"));


app.use('/api', router);

// Run app
app.listen(3000, () => {
  console.log("Express application is up ansd running on port 3000");
});
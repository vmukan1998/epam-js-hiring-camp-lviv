import UserController from "../controller/UserController";
import { Router, Request, Response } from "express";

const router = Router();

router.get('/hc', (req: Request, res: Response) => {
  res.json({status: 'ok'});
})

export default router;
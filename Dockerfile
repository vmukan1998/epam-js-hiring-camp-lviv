FROM node:9.11.1-alpine
COPY package.json ./

RUN npm install --production

COPY src/ ./
ENTRYPOINT npm start